"""Top-level package for Example GitLab Python Project."""

__author__ = """Antoine Collet"""
__email__ = 'antoine.collet5@gmail.com'
__version__ = '0.1.0'

from .plotters import NestedGridPlotter

__all__ = ['NestedGridPlotter', ]
