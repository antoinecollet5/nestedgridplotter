"""
Contains plotters classes.

These classes allows to wrap the creation of figures with matplotlib and to use
a unified framework.

Created on Mon Sep 16 06:52:36 2019

@author: ancollet
"""

import numpy as np
import warnings
import re
from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec
from matplotlib.lines import Line2D
from dateutil import relativedelta
from datetime import timedelta
from collections import OrderedDict
from itertools import product


def get_line_style(line_style_label):
    """
    Get a parametrized linestyle from a line style label.

    See
    https://matplotlib.org/stable/gallery/lines_bars_and_markers/linestyles.html

    Parameters
    ----------
    line_style_label : str
        Desired line style label.

    Returns
    -------
    tuple(int)
        The line style parameters.

    """
    LINE_STYLES = OrderedDict([
        ('solid', (0, ())),
        ('loosely dotted', (0, (1, 10))),
        ('dotted', (0, (1, 5))),
        ('densely dotted', (0, (1, 1))),
        ('loosely dashed', (0, (5, 10))),
        ('dashed', (0, (5, 5))),
        ('densely dashed', (0, (5, 1))),
        ('loosely dashdotted', (0, (3, 10, 1, 10))),
        ('dashdotted', (0, (3, 5, 1, 5))),
        ('densely dashdotted', (0, (3, 1, 1, 1))),
        ('loosely dashdotdotted', (0, (3, 10, 1, 10, 1, 10))),
        ('dashdotdotted', (0, (3, 5, 1, 5, 1, 5))),
        ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))]
    )
    return LINE_STYLES.get(line_style_label)


class NestedGridPlotter:
    """General class to wrap matplotlib plots."""

    def __init__(self, figsize=None, dpi=None, facecolor=None, edgecolor=None,
                 linewidth=0.0, frameon=None, subplotpars=None,
                 tight_layout=None, constrained_layout=None,
                 nrows=1, ncols=1, figure=None, left=None, bottom=None,
                 right=None, top=None, wspace=None,
                 hspace=None, width_ratios=None, height_ratios=None,
                 nested_gridspec_params=None):
        """
        Construct the Plotter class.

        Parameters
        ----------
        figsize : 2-tuple of floats, default: :rc:`figure.figsize`
            Figure dimension ``(width, height)`` in inches.

        dpi : float, default: :rc:`figure.dpi`
            Dots per inch.

        facecolor : default: :rc:`figure.facecolor`
            The figure patch facecolor.

        edgecolor : default: :rc:`figure.edgecolor`
            The figure patch edge color.

        linewidth : float
            The linewidth of the frame (i.e. the edge linewidth of the figure
            patch).

        frameon : bool, default: :rc:`figure.frameon`
            If ``False``, suppress drawing the figure background patch.

        subplotpars : `SubplotParams`
            Subplot parameters. If not given, the default subplot
            parameters :rc:`figure.subplot.*` are used.

        tight_layout : bool or dict, default: :rc:`figure.autolayout`
            If ``False`` use *subplotpars*. If ``True`` adjust subplot
            parameters using `.tight_layout` with default padding.
            When providing a dict containing the keys ``pad``, ``w_pad``,
            ``h_pad``, and ``rect``, the default `.tight_layout` paddings
            will be overridden.

        constrained_layout : bool, default: :rc:`figure.constrained_layout.use`
            If ``True`` use constrained layout to adjust positioning of plot
            elements.  Like ``tight_layout``, but designed to be more
            flexible.  See
            :doc:`/tutorials/intermediate/constrainedlayout_guide`
            for examples.  (Note: does not work with `add_subplot` or
            `~.pyplot.subplot2grid`.)

        nrows, ncols : int
            The number of rows and columns of the grid.

        left, right, top, bottom : float, optional
            Extent of the subplots as a fraction of figure width or height.
            Left cannot be larger than right, and bottom cannot be larger than
            top. If not given, the values will be inferred from a figure or
            rcParams at draw time. See also `GridSpec.get_subplot_params`.

        wspace : float, optional
            The amount of width reserved for space between subplots,
            expressed as a fraction of the average axis width.
            If not given, the values will be inferred from a figure or
            rcParams when necessary. See also `GridSpec.get_subplot_params`.

        hspace : float, optional
            The amount of height reserved for space between subplots,
            expressed as a fraction of the average axis height.
            If not given, the values will be inferred from a figure or
            rcParams when necessary. See also `GridSpec.get_subplot_params`.

        width_ratios : array-like of length *ncols*, optional
            Defines the relative widths of the columns. Each column gets a
            relative width of ``width_ratios[i] / sum(width_ratios)``.
            If not given, all columns will have the same width.

        height_ratios : array-like of length *nrows*, optional
            Defines the relative heights of the rows. Each column gets a
            relative height of ``height_ratios[i] / sum(height_ratios)``.
            If not given, all rows will have the same height.

        nested_gridspec_params: dict, optional,
            Default is None.


        Example of sub_gridspec_params
        ------------------------------
        https://matplotlib.org/stable/gallery/userdemo/demo_gridspec06.html#sphx-glr-gallery-userdemo-demo-gridspec06-py
        sub_gridspec_params = {
            0: {
                "dim": (3, 3),
                "subgrid_spec": [(slice(None, -1), slice(None)),
                                 (-1, slice(None, -1)),
                                 (-1, -1)
                                 ],
                "sharey": False
                },
            1: {
                "dim": (3, 3),
                "subgrid_spec": [(slice(None), slice(None, -1)),
                                 (slice(None, -1), -1),
                                 [-1, -1]],
                "sharey": False
                }
            }

        Returns
        -------
        None.

        """
        self.nrows = nrows
        self.ncols = ncols
        self.figure_params = dict(
            figsize=figsize, dpi=dpi, facecolor=facecolor, edgecolor=edgecolor,
            linewidth=linewidth, frameon=frameon, subplotpars=subplotpars,
            tight_layout=tight_layout, constrained_layout=constrained_layout
            )
        self.gridspec_params = dict(
            nrows=nrows, ncols=ncols, left=left, bottom=bottom, right=right,
            top=top, wspace=wspace, hspace=hspace, width_ratios=width_ratios,
            height_ratios=height_ratios
            )
        self.nested_gridspec_params = nested_gridspec_params
        # create the figure and its axes
        self._initiate_fig()

    @property
    def nested_gridspec_params(self):
        """Get nested gridspec parameters."""
        return self._nested_gridspec_params

    @nested_gridspec_params.setter
    def nested_gridspec_params(self, params):
        """Set nested gridspec parameters."""
        if params is None:
            self._nested_gridspec_params = {}
        else:
            self._nested_gridspec_params = params

    def _initiate_fig(self):
        """Create the Plotter embedded figure.

        **kwargs : optional
            See  `plt.figure` for other possible arguments.
        """
        self.fig = plt.figure(**self.figure_params)
        # Two dict to store the handles and labels to add to the legend
        # The key is the number of the id of the axis matching the handles
        self.additional_handles = {}
        self.additional_labels = {}
        # avoid the figure display
        plt.close()
        self._make_subplots()

    def _make_subplots(self):
        """Make the subplots of the figure."""
        # Empty list
        self.axes = []
        self.subplots = []
        # Setting the grid
        self.outer_gs = GridSpec(figure=self.fig, **self.gridspec_params)
        # if a specific gridspec_params is provided (subplots that span
        # rows and columns)
        # for out_index, (i, j) in enumerate(coordinates):
        for index in range(self.nrows * self.ncols):
            sub_gridspec_params = self.nested_gridspec_params.get(index, {})
            self._create_subplot_axes(index, sub_gridspec_params)

    def _create_subplot_axes(self, index, sub_gridspec_params):
        """
        Create axes for the subplot of the outer grid.

        If no gridspec_params is given, then one subplot is created per cell
        of the grid, giving a regular structure.

        Parameters
        ----------
        index : int
            Index of the axis in the outer_grid.
        sub_gridspec_params : dict
            Parameters for the nested grid in the outer grid.
            Expected keywords are "dim" as tuple(int), "subgrid_spec" as a
            list of list of list of tuple of slice or int, "sharey" and
            "sharex" as booleans. The default is {}.

        Returns
        -------
        None.
        """
        dim = sub_gridspec_params.get("dim", (1, 1))
        in_gs_kwargs = sub_gridspec_params.get("in_gs_kwargs", {})
        # Creatin of the inner grid
        in_gs_kwargs.update({"subplot_spec": self.outer_gs[index]})
        inner_gs = GridSpecFromSubplotSpec(*dim, **in_gs_kwargs)
        # Create a wrapper of the grid spec for a common x/y labels
        # Must be added before the subplots
        self.subplots.append(
            self._add_gridspec_wrapping_subplot(self.outer_gs[index]))

        subgrid_spec = sub_gridspec_params.get("subgrid_spec", None)
        if subgrid_spec:
            for specs in subgrid_spec:
                # use tuple to handle list as well
                self.axes.append(self.fig.add_subplot(inner_gs[tuple(specs)]))
        else:
            for row, column in product(range(dim[0]), range(dim[1])):
                subplot_kwargs = self._get_share_params(row, column, dim,
                                                        sub_gridspec_params)
                ax_id = self._get_axis_id_from_axis_row_and_column(row, column,
                                                                   dim[1])
                self.axes.append(self.fig.add_subplot(inner_gs[ax_id],
                                                      **subplot_kwargs)
                                 )
                self._hide_ticklabels_for_shared_axes(row, column, dim,
                                                      sub_gridspec_params)

    def _add_gridspec_wrapping_subplot(self, gs_subplotspec):
        """
        Add a subplot wrapping the subgrid.

        This is usefull to add wrapping titles, legend or axis titles.

        Parameters
        ----------
        gs_subplotspec : gridspec.SubplotSpec
            Specifies the location of a subplot in a GridSpec.

        Returns
        -------
        ax : axes.subplots.AxesSubplot
            A subgrid wrapping subplot.

        """
        ax = self.fig.add_subplot(gs_subplotspec)
        # Turn off axis lines and ticks of the big subplot
        self.make_patch_spines_invisible(ax)
        self.hide_axis_ticks_and_ticklabels(ax, which="both")
        return ax

    # @plt._copy_docstring_and_deprecators(plt.savefig)
    def savefig(self, *args, **kwargs):
        """Save the current figure."""
        res = self.fig.savefig(*args, **kwargs)
        # need this if 'transparent=True' to reset colors
        self.fig.canvas.draw_idle()
        return res

    @staticmethod
    def _get_axis_id_from_axis_row_and_column(row, column, nb_columns):
        """Return the ax_id from the axis row and column # on the grid."""
        return row * nb_columns + column

    def _get_share_params(self, row, column, dim, sub_gridspec_params):
        """Get the sharex and sharey params for the figure subgrid."""
        sharex = None
        sharey = None
        if row > 0 and sub_gridspec_params.get("sharex", False):
            sharex = self.axes[
                self._get_axis_id_from_axis_row_and_column(row - 1, column,
                                                           dim[1])
                ]
        if column > 0 and sub_gridspec_params.get("sharey", False):
            sharey = self.axes[
                self._get_axis_id_from_axis_row_and_column(row, column - 1,
                                                           dim[1])
                ]
        return {"sharex": sharex, "sharey": sharey}

    def _hide_ticklabels_for_shared_axes(self, row, column, dim, kwargs):
        """
        Hide the thicklabels of the shared axis.

        - *sharex*: only the thicklabels of the last ax_id are kept
        - *sharey*: only the thicklabels of the first ax_id are kept

        Parameters
        ----------
        ax_id : int
            Index of the axis to modify in the axes list.
        kwargs: dict
            Dictionnary of parameters which might contain sharex and sharey
            booleans.

        Returns
        -------
        None.

        """
        ax_id = self._get_axis_id_from_axis_row_and_column(row, column, dim[1])
        if kwargs.get("sharex", False) and row < dim[0] - 1:
            self.hide_axis_ticklabels(self.axes[ax_id], which='x')
        if kwargs.get("sharey", False) and column > 0:
            self.hide_axis_ticklabels(self.axes[ax_id], which='y')

    @staticmethod
    def make_patch_spines_invisible(ax):
        """
        Make patch and spines of the ax invisible.

        Usefull for creating a 2nd twin-x axis on the right/left.

        Parameters
        ----------
        ax : mpl.Axis
            Axis to modify.

        Example
        -------
            fig, ax=plt.subplots()
            ax.plot(x, y)
            tax1=ax.twinx()
            tax1.plot(x, y1)
            tax2=ax.twinx()
            tax2.spines['right'].set_position(('axes',1.09))
            make_patch_spines_invisible(tax2)
            tax2.spines['right'].set_visible(True)
            tax2.plot(x, y2)
        """
        ax.set_frame_on(True)
        ax.patch.set_visible(False)
        for sp in ax.spines.values():
            sp.set_visible(False)

    @staticmethod
    def replace_bad_path_characters(self, filename, newvalue="_"):
        """
        Make filename compatible with path by replacement.

        Replace anything that isn't alphanumeric, -, _, a space, or a period.

        Parameters
        ----------
        filename : str
            Old filename.
        newvalue : str
            The string to replace the bad values with.

        Returns
        -------
        str
            New filename.

        """
        return re.sub(r'[^\w\-_\. ]', newvalue, filename)

    def add_grid_to_axis(self, ax_id=0, which="both", alpha=0.5):
        """
        Add a grey grid with inner black ticks on the four edges.

        Parameters
        ----------
        ax_id : int, optional
            Index of the axis to modify in the axes list. The default is 0.
        """
        # Add the grid
        self.axes[ax_id].grid(color='lightgrey',
                              which=which,
                              linestyle=get_line_style('dotted'),
                              linewidth=1, alpha=alpha)
        # Make the ticks inner and on the four edges of the figure box
        self.axes[ax_id].tick_params(direction='in', length=10, width=1.5,
                                     which=which,
                                     colors='k', grid_color='k',
                                     grid_alpha=alpha, top=True, right=True,
                                     bottom=True, left=True)

    def add_grid_to_all_axes(self, **kwargs):
        """Add a grid to all the axes of the figure."""
        for ax_id in range(len(self.axes)):
            self.add_grid_to_axis(ax_id, **kwargs)

    def get_axis_legend_items(self, ax_id):
        """Get the axis legend items."""
        handles, labels = self.axes[ax_id].get_legend_handles_labels()
        # Add the additional handles and labels of the axis
        handles += self.additional_handles.get(ax_id, [])
        labels += self.additional_labels.get(ax_id, [])
        return handles, labels

    def remove_dulicated_legend_items(self, handles, labels):
        """Remove the duplicated legend handles and labels."""
        # remove the duplicates
        by_label = dict(zip(labels, handles))
        return by_label.values(), by_label.keys()

    def gather_figure_legend_items(self, remove_duplicates=True):
        """Gather the legend items from all axes."""
        self.handles, self.labels = [], []
        for ax_id, ax in enumerate(self.axes):
            hdl, lab = self.get_axis_legend_items(ax_id)
            self.handles += hdl
            self.labels += lab

        # remove the duplicates
        if remove_duplicates:
            self.handles, self.labels = (
                self.remove_dulicated_legend_items(self.handles, self.labels))

        return self.handles, self.labels

    def add_fig_legend(self, legend_font_size=16, legend_y_shift=0.05,
                       legend_ncol=2, **kwargs):
        """Add a legend to the graphic."""
        self.gather_figure_legend_items()
        if len(self.handles) == 0:
            return
        axbox = self.axes[-1].get_position()
        self.fig.legend(self.handles, self.labels, loc='center',
                        ncol=legend_ncol, fontsize=legend_font_size,
                        bbox_to_anchor=[0.5, axbox.y0 - legend_y_shift],
                        bbox_transform=self.fig.transFigure, **kwargs)

    def add_axis_legend(self, ax_id, legend_font_size=16, legend_ncol=1,
                        loc='best', **kwargs):
        """Add a legend to the graphic."""
        handles, labels = self.remove_dulicated_legend_items(
            *self.get_axis_legend_items(ax_id))

        self.axes[ax_id].legend(handles, labels, loc=loc,
                                ncol=legend_ncol, fontsize=legend_font_size,
                                **kwargs)

    def add_subplot_title(self, subplot_id=0, title=None, **kwargs):
        """If available, add a title to the subplot."""
        if title:
            self.subplots[subplot_id].set_title(title, **kwargs)

    def add_axis_title(self, ax_id=0, title=None, **kwargs):
        """If available, add a title to the axis."""
        if title:
            self.axes[ax_id].set_title(title, **kwargs)

    def set_axis_range_start_at_zero(self, ax_id=0):
        """
        Change x and y axes range to start at zero.

        Parameters
        ----------
        ax_id : int, optional
            id of the axis to modify. The default is 0.
        """
        self.axes[ax_id].set_xlim(left=0.0)
        self.axes[ax_id].set_ylim(bottom=0.0)

    def set_all_axis_range_start_at_zero(self):
        """Change x and y axes range to start at zero for all axes."""
        for ax_id in range(len(self.axes)):
            self.set_axis_range_start_at_zero(ax_id)

    def subplots_adjust(self, **kwargs):
        """Adjust the subplot layout parameters."""
        self.fig.subplots_adjust(**kwargs)

    def clear_all_axes(self):
        """Clear all the axes from their data and curves."""
        for ax in self.axes:
            ax.clear()
        # Also clear the elements of the legend
        self.additional_handles = {}
        self.additional_labels = {}

    @staticmethod
    def hide_axis_ticklabels(axis, which="both"):
        """Hide the y ticklabels of the axis."""
        if which in ["both", 'x']:
            plt.setp(axis.get_xticklabels(), visible=False)
        if which in ["both", 'y']:
            plt.setp(axis.get_yticklabels(), visible=False)

    @staticmethod
    def hide_axis_ticks(axis, which="both"):
        """Hide the y ticklabels of the axis."""
        if which in ["both", 'x']:
            axis.tick_params(left=False, right=False)
        if which in ["both", 'y']:
            axis.tick_params(bottom=False, top=False)

    @staticmethod
    def hide_axis_ticks_and_ticklabels(axis, which="both"):
        """Hide the tick and ticklabels of the axis."""
        if which in ["both", 'x']:
            axis.set_xticklabels([])
            axis.tick_params(bottom=False, top=False)
        if which in ["both", 'y']:
            axis.set_yticklabels([])
            axis.tick_params(left=False, right=False)

    def hide_all_y_ticklabels(self):
        """Hide the y ticklabels of all axes."""
        for ax in self.axes:
            self.hide_axis_y_ticklabels(ax)

    def hide_all_y_ticks_and_ticklabels(self):
        """Hide the y ticklabels of all axes."""
        for ax in self.axes:
            self.hide_axis_y_tick_and_ticklabels(ax)

    def adjust_font_size(self, fontsize=20):
        """Adjust the font size to a single or two columns document."""
        plt.rcParams.update({'font.size': fontsize})

    def hide_axis_spine(self, ax, loc='top'):
        """Hide one of the spine of the axis."""
        ax.spines[loc].set_visible(False)

    def hide_axis_spines(self, ax):
        """Hide all the spine of an axis."""
        for loc in ['right', 'left', 'bottom', 'top']:
            self.hide_axis_spine(loc)

    @staticmethod
    def add_line_to_axis(axis, pos, orientation='v', xmin=0.0, xmax=1.0,
                         ymin=0.0, ymax=1.0, linestyle='--', c='blue',
                         linewidth=1.5):
        """
        Add a line to the axis and return its handle.

        Parameters
        ----------
        axis : TYPE
            DESCRIPTION.
        pos : TYPE
            DESCRIPTION.
        orientation : TYPE, optional
            DESCRIPTION. The default is 'v'.
        xmin : TYPE, optional
            DESCRIPTION. The default is 0.0.
        xmax : TYPE, optional
            DESCRIPTION. The default is 1.0.
        ymin : TYPE, optional
            DESCRIPTION. The default is 0.0.
        ymax : TYPE, optional
            DESCRIPTION. The default is 1.0.
        linestyle : TYPE, optional
            DESCRIPTION. The default is '--'.
        c : TYPE, optional
            DESCRIPTION. The default is 'blue'.
        linewidth : TYPE, optional
            DESCRIPTION. The default is 1.5.

        Returns
        -------
        handle : TYPE
            DESCRIPTION.
        """
        if orientation == 'v':
            axis.axvline(x=pos, ymin=ymin, ymax=ymax, linestyle=linestyle,
                         c=c, lw=linewidth)
        if orientation == 'h':
            axis.axhline(y=pos, xmin=xmin, xmax=xmax, linestyle=linestyle,
                         c=c, lw=linewidth)
        handle = Line2D([0, 0], [0, 1], color=c, linestyle=linestyle,
                        lw=linewidth)
        return handle

    @staticmethod
    def align_yaxes_ticks(ax1, ax2):
        """
        Align the ticks of two axes with different data and scales.

        Align zeros of the two axes, zooming them out by same ratio

        Note
        ----
        code coming from:
        https://stackoverflow.com/questions/10481990/matplotlib-axis-with-two-scales-shared-origin

        """
        y_lims = np.array([ax.get_ylim() for ax in [ax1, ax2]],
                          dtype='float64')

        # force 0 to appear on both axes
        y_lims[:, 0] = y_lims[:, 0].clip(None, 0)
        y_lims[:, 1] = y_lims[:, 1].clip(0, None)

        # normalize both axes
        y_mags = (y_lims[:, 1] - y_lims[:, 0]).reshape(len(y_lims), 1)
        y_lims_normalized = y_lims / y_mags

        # find combined range
        y_new_lims_normalized = np.array([np.min(y_lims_normalized),
                                          np.max(y_lims_normalized)],
                                         dtype='float64')

        # denormalize combined range to get new axes
        new_lim1, new_lim2 = y_new_lims_normalized * y_mags
        ax1.set_ylim(new_lim1)
        ax2.set_ylim(new_lim2)

    @staticmethod
    def align_y_axes(axes, align_values=None, is_ticks_major=True):
        """
        Align the ticks of multiple y axes.

        A new sets of ticks are computed for each axis in <axes> but with equal
        length.
        Source :
        https://stackoverflow.com/questions/26752464/how-do-i-align-gridlines-for-two-y-axis-scales-using-matplotlib

        Parameters
        ----------
        axes : list
            List of axes objects whose yaxis ticks are to be aligned.
        align_values : None or list/tuple
            If not None, should be a list/tuple of floats with same length as
            <axes>. Values in <align_values> define where the corresponding
            axes should be aligned up. E.g. [0, 100, -22.5] means the 0 in
            axes[0], 100 in axes[1] and -22.5 in axes[2] would be aligned up.
            If None, align (approximately) the lowest ticks in all axes.

        Returns
        -------
            new_ticks (list): a list of new ticks for each axis in <axes>.
        """
        from matplotlib.pyplot import MaxNLocator

        nax = len(axes)
        if not is_ticks_major:
            ticks = [aii.get_yticks() for aii in axes]
        else:
            ticks = [aii.get_yticks(minor=False) for aii in axes]
            max_nbins = max([len(tick) for tick in ticks])
            print(max_nbins)
        if align_values is None:
            aligns = [ticks[ii][0] for ii in range(nax)]
        elif len(align_values) != nax:
            raise Exception("Length of <axes> doesn't equal that "
                            "of <align_values>.")
        else:
            aligns = align_values

        bounds = [aii.get_ylim() for aii in axes]

        # align at some points
        ticks_align = [ticks[ii] - aligns[ii] for ii in range(nax)]

        # scale the range to 1-100
        ranges = [tii[-1]-tii[0] for tii in ticks]
        lgs = [-np.log10(rii)+2. for rii in ranges]
        igs = [np.floor(ii) for ii in lgs]
        log_ticks = [ticks_align[ii]*(10.**igs[ii]) for ii in range(nax)]

        # put all axes ticks into a single array,
        # then compute new ticks for all
        comb_ticks = np.concatenate(log_ticks)
        comb_ticks.sort()
        locator = MaxNLocator(nbins='auto', steps=[1, 2, 2.5, 3, 4, 5, 8, 10])
        new_ticks = locator.tick_values(comb_ticks[0], comb_ticks[-1])
        new_ticks = [new_ticks/10.**igs[ii] for ii in range(nax)]
        new_ticks = [new_ticks[ii]+aligns[ii] for ii in range(nax)]

        # find the lower bound
        idx_l = 0
        for i in range(len(new_ticks[0])):
            if any([new_ticks[jj][i] > bounds[jj][0] for jj in range(nax)]):
                idx_l = i-1
                break

        # find the upper bound
        idx_r = 0
        for i in range(len(new_ticks[0])):
            if all([new_ticks[jj][i] > bounds[jj][1] for jj in range(nax)]):
                idx_r = i
                break

        # trim tick lists by bounds
        new_ticks = [tii[idx_l:idx_r+1] for tii in new_ticks]

        # set ticks for each axis
        for axii, tii in zip(axes, new_ticks):
            axii.set_yticks(tii)

        return new_ticks


class TemporalGraphPlotter(NestedGridPlotter):
    """General class to create temporal 2D graphs."""

    def __init__(self, nb_rows=1, nb_cols=1, gridspec_params=None,
                 h_size=13, v_size=7, **kwargs):
        """
        Construct the class.

        Parameters
        ----------
        nb_axes : TYPE
            DESCRIPTION.
        h_size : TYPE
            DESCRIPTION.
        v_size : TYPE
            DESCRIPTION.
        **kwargs : dict
            All keywords arguments used by the superclass.

        Returns
        -------
        None.

        """
        super().__init__(nb_rows, nb_cols, gridspec_params, h_size, v_size,
                         **kwargs)
        self.axes_idx = {}

    def add_dates_to_second_date_axis(self, ax1, ax2, idx, time_unit='days',
                                      position=38, date_rotation=15):
        """
        Add dates to an already existing axis.

        The dates are creating frm a first day axis (numbered from x to n),
        taking the time series first date as the strating date.
        """
        # Get the xticks of the first x axis
        ax1Xs = ax1.get_xticks()
        # Create an empty list of the future ticks for the second x-axis
        ax2Xs = []
        # Fill the list
        if time_unit == 'days':
            for X in ax1Xs:
                ax2Xs.append((idx[0]
                              + timedelta(days=float(X)))
                             .strftime('%d-%m-%Y')
                             )
        elif time_unit == 'months':
            for X in ax1Xs:
                ax2Xs.append((idx[0] + relativedelta(
                    months=float(X))).strftime('%m-%Y')
                    )
        elif time_unit == 'years':
            for X in ax1Xs:
                ax2Xs.append((idx[0] + relativedelta(
                    years=float(X))).strftime('%Y')
                    )
        else:
            raise ValueError("date_rotation should be in ['days', "
                             "'months', 'years']")

        ax2.set_xticks(ax1Xs)
        ax2.set_xbound(ax1.get_xbound())
        ax2.set_xticklabels(ax2Xs)

        ax1.xaxis.set_ticks_position('bottom')
        ax2.xaxis.set_ticks_position('bottom')
        ax2.spines['bottom'].set_position(('outward', position))

        for tick in ax2.get_xticklabels():
            tick.set_rotation(date_rotation)

    def add_date_axis(self, idx=None, ax_id=0, position=48):
        """Add additionnal date axis under the existing x axis."""
        if idx is None:
            warnings.warn("Unable to create a date x_axis for the axis "
                          f"{ax_id}: empty axis !")
            return

        # Creation of a second x axis
        ax2 = self.axes[ax_id].twiny()

        # Settingt the second axis
        self.add_dates_to_second_date_axis(self.axes[ax_id], ax2, idx,
                                           position=position)

    def plot_u_hm_daily(self, data, ax_id=0, label='U production (kg/d) HM',
                        color='#d62728', zorder=1, alpha=1,
                        marker=None, linestyle='-', **kwargs):
        """Add the daily hm uranium production to the graph."""
        self.axes[ax_id].plot(data, marker=marker, linestyle=linestyle,
                              c=color, label=label, zorder=zorder,
                              alpha=alpha, **kwargs)

    def plot_u_hm_cum(self, data, ax_id=0, label='U cum production (t) HM',
                      c='#d62728', zorder=1, alpha=1, marker=None,
                      linestyle="--", **kwargs):
        """Add the cumulated hm uranium production to the graph."""
        self.axes[ax_id].plot(data, marker=marker,
                              linestyle=linestyle,
                              label=label, zorder=zorder, alpha=alpha, c=c)

    def plot_u_fact_daily(self, data, ax_id=0, label='U production (kg/d) HM',
                          marker='x', markersize='6', linestyle='None',
                          mfc='#1f77b4', mec='#1f77b4', c='#1f77b4',
                          zorder=0, linewidth=2, m_alpha=0.5, l_alpha=0.5,
                          **kwargs):
        """Add the daily factual uranium production to the graph."""
        # Plot the markers
        self.axes[ax_id].plot(data, marker=marker, markersize=markersize,
                              linestyle="None", mfc=mfc, mec=mec, label=label,
                              zorder=zorder, alpha=m_alpha)
        # Plot the line
        if linestyle == 'None':
            return
        self.axes[ax_id].plot(data, linestyle=linestyle, linewidth=linewidth,
                              c=c, zorder=zorder, alpha=l_alpha, label=label)

    def plot_u_fact_cum(self, data, ax_id=0, marker=None,
                        linestyle="--",
                        color='#1f77b4', linewidth=3,
                        label='U_Cumulated_Production_(t)_HM',
                        zorder=0, alpha=0.7, **kwargs):
        """Add the cumulated factual uranium production to the graph."""
        self.axes[ax_id].plot(data, marker=marker,
                              linestyle=linestyle,
                              c=color, linewidth=linewidth,
                              label=label, zorder=zorder, alpha=alpha)

    def plot_u_pred_daily(self, data, ax_id=0, label='U prod. (kg/d) PRED',
                          marker=None,
                          linestyle=get_line_style('densely dashed'),
                          zorder=2, color='k', linewidth=2, **kwargs):
        """Add the predicted uranium daily production to the graph."""
        self.axes[ax_id].plot(data, marker=marker, linestyle=linestyle,
                              c=color, label=label, zorder=zorder,
                              linewidth=linewidth)

    def plot_u_pred_cum(self, data, ax_id=0, label='U prod. cum. (t) PRED',
                        marker=None, linestyle=get_line_style('dashed'),
                        zorder=2, color='k', **kwargs):
        """Add the predicted uranium cumulated production to the graph."""
        self.axes[ax_id].plot(data, marker=marker, c=color,
                              linestyle=linestyle,
                              label=label, zorder=zorder)

    def plot_acid_hm_daily(self, data, ax_id=0, label='U production (kg/d) HM',
                           color='k', zorder=1, alpha=.7, linewidth=1.5,
                           marker=None, linestyle='-', **kwargs):
        """Add the daily hm uranium production to the graph."""
        self.axes[ax_id].plot(data, marker=marker, linestyle=linestyle,
                              c=color, label=label, zorder=zorder,
                              linewidth=linewidth,
                              alpha=alpha, **kwargs)

    def plot_acid_hm_cum(self, data, ax_id=0, linewidth=2,
                         label='acid cum production (t) HM',
                         c='k', zorder=1, alpha=.8, marker=None,
                         linestyle=get_line_style('dashed'), **kwargs):
        """Add the cumulated hm uranium production to the graph."""
        self.axes[ax_id].plot(data, marker=marker,
                              linestyle=linestyle,
                              label=label, zorder=zorder, alpha=alpha, c=c)

    def plot_acid_fact_daily(self, data, ax_id=0,
                             label='acid production (kg/d) HM',
                             marker='x', markersize='6', linestyle='None',
                             mfc=None, mec='indianred', c=None,  # '#1f77b4'
                             zorder=0, linewidth=.5, alpha=.7, **kwargs):
        """Add the daily factual uranium production to the graph."""
        self.axes[ax_id].plot(data, marker=marker, markersize=markersize,
                              linestyle=linestyle,
                              mfc=mfc, mec=mec, c=c, label=label,
                              zorder=zorder, linewidth=linewidth, alpha=alpha)

    def plot_acid_fact_cum(self, data, ax_id=0, marker=None,
                           linestyle=get_line_style('dashed'),
                           color='indianred', linewidth=3,
                           label='U_Cumulated_Production_(t)_HM',
                           zorder=0, alpha=0.8, **kwargs):
        """Add the cumulated factual uranium production to the graph."""
        self.axes[ax_id].plot(data, marker=marker,
                              linestyle=linestyle,
                              c=color, linewidth=linewidth,
                              label=label, zorder=zorder, alpha=alpha)

    def plot_pH_hm(self, data, ax_id=0, label='pH simulated',
                   color='k', zorder=1, alpha=1,
                   marker=None, linestyle='-', **kwargs):
        """Add the daily hm uranium production to the graph."""
        self.axes[ax_id].plot(data, marker=marker, linestyle=linestyle,
                              c=color, label=label, zorder=zorder,
                              alpha=alpha, **kwargs)

    def plot_pH_fact(self, data, ax_id=0, label='pH factual',
                     marker='+', markersize='6', linestyle='None',
                     mfc='#2ca02c', mec='#2ca02c', c='#2ca02c',
                     zorder=0, linewidth=1, alpha=.1, **kwargs):
        """Add the daily factual uranium production to the graph."""
        self.axes[ax_id].plot(data, marker=marker, markersize=markersize,
                              linestyle=linestyle,
                              mfc=mfc, mec=mec, c=c, label=label,
                              zorder=zorder, linewidth=linewidth, alpha=alpha)

    def add_subplot_abscissa_title(self, ax_id):
        """Add the different axis titles."""
        # Axis titles
        self.axes[ax_id].set_xlabel("Time (days from start)",
                                    fontweight="bold")

    def add_axis_abscissa_title(self, ax_id):
        """Add the different axis titles."""
        # Axis titles
        self.axes[ax_id].set_xlabel("Time (days from start)",
                                    fontweight="bold")

    def add_axis_ordinate_title(self, ax_id=0, ax_ylabel=None):
        """Add the different axis titles."""
        if not ax_ylabel:
            return
        else:
            self.axes[ax_id].set_ylabel(ax_ylabel, fontweight="bold")

    def add_all_date_axis(self):
        """Add a date xaxis for all axes of the figure."""
        for ax_id in range(int(len(self.axes) / 2)):
            self.add_date_axis(self.axes_idx.get(ax_id, None), ax_id)
