=============================
Nested Grid Plotter
=============================

Nested Grid Plotter is based on matplotlib and intends to simplify the plotting of nestedgrid by providing an objected oriented class.

.. image:: https://gitlab.com/antoinecollet5/nestedgridplotter/badges/master/pipeline.svg
    :target: https://gitlab.com/antoinecollet5/nestedgridplotter/pipelines/
    :alt: Build Status

.. image:: https://img.shields.io/pypi/v/nestedgridplotter.svg
    :target: https://pypi.org/pypi/nestedgridplotter
    :alt: PyPI

.. image:: https://gitlab.com/antoinecollet5/nestedgridplotter/badges/master/coverage.svg
    :target: https://gitlab.com/antoinecollet5/pipelines/
    :alt: Coverage


.. image:: https://readthedocs.org/projects/nestedgridplotter/badge/?version=latest
        :target: https://nestedgridplotter.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


.. image:: https://img.shields.io/badge/License-MIT license-blue.svg
    :target: https://gitlab.com/antoinecollet5/nestedgridplotter/-/blob/master/LICENSE


* Free software: MIT license
* Documentation: https://nestedgridplotter.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `cookiecutter-pypackage-gitlab`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-pypackage-gitlab`: https://gitlab.com/AdriaanRol/cookiecutter-pypackage-gitlab
