Welcome to Nested Grid Plotter's documentation!
===============================================

.. include:: readme.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   installation
   usage
   contributing
   authors
   changelog

API Reference
=============

Contents:

.. toctree::
   :maxdepth: 2
   :caption: API reference

   api_reference


Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
