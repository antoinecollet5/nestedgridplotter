import pytest
from nestedgridplotter import NestedGridPlotter
import matplotlib.pyplot as plt
new_rc_params = {
    "font.family": 'sans-serif',
    "font.sans-serif": ['Helvetica', 'DejaVu Sans'],
    "font.size": 16,
    "text.usetex": False,
    "savefig.format": 'svg',
    "svg.fonttype": 'none',  # to store text as text, not as path
    "savefig.facecolor": "w",
    "savefig.edgecolor": "k",
    "savefig.dpi": 300}
csfont = {'fontname': 'Comic Sans MS'}
hfont = {'fontname': 'Helvetica'}
plt.rcParams.update(new_rc_params)


@pytest.fixture
def tmp_folder(tmp_path_factory):
    """Create a temporary directory"""
    return tmp_path_factory.mktemp('tmp_folder')


def test_regular_grid(tmp_folder):
    """Test regular capabilities."""
    plotter = NestedGridPlotter(nrows=3, ncols=3)
    plotter.savefig(fname="test_regular_grid", save_path=tmp_folder,
                    bbox_inches='tight')


def test_nested_gridspecs(tmp_folder):
    """
    Test Nested Gridspecs capabilities.

    Reproduce:
    https://matplotlib.org/stable/gallery/userdemo/demo_gridspec06.html#sphx-glr-gallery-userdemo-demo-gridspec06-py
    """
    nested_gridspec_params = {
        0: {
            "dim": (3, 3),
            "subgrid_spec": [(slice(None, -1), slice(None)),
                             (-1, slice(None, -1)),
                             (-1, -1)
                             ],
            "sharey": False
            },
        1: {
            "dim": (3, 3),
            "subgrid_spec": [(slice(None), slice(None, -1)),
                             (slice(None, -1), -1),
                             [-1, -1]],
            "sharey": False
            }
        }

    plotter = NestedGridPlotter(figsize=(15, 15), nrows=1, ncols=2,
                                nested_gridspec_params=nested_gridspec_params)
    plotter.savefig(fname="test_nested_gridspecs", save_path=tmp_folder,
                    bbox_inches='tight')


def test_shared_axes(tmp_folder):
    """Test shared axes capabilities."""
    nested_gridspec_params = {
        0: {
            "dim": (3, 4),
            "sharex": True,
            "sharey": True,
            }
        }
    plotter = NestedGridPlotter(figsize=(15, 15), nrows=1, ncols=1,
                                nested_gridspec_params=nested_gridspec_params,
                                facecolor='w', edgecolor='k')
    # plotter.subplots[0].set_xlabel("Test_subplot x label", fontweight="bold", y=0.2)
    plotter.subplots[0].text(0.5, 0.5, "Test_subplot", zorder=4,
                             transform=plotter.subplots[0].transAxes,
                             fontweight="bold")
    plotter.subplots[0].set_ylabel("Test subplot y label", fontweight="bold")
    plotter.fig.align_labels()
    plotter.savefig(fname="test_shared_axes", save_path=tmp_folder,
                    bbox_inches='tight')